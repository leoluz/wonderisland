Wonder Island Booking Service
=============================

This micro-service provides back-end functionalities for managing reservations for Wonder Island.

Technologies Overview
---------------------

This project is built with the following technologies:

* [OpenAPI/Swagger][1]: For REST API tooling.
* [Spring Boot][2]: The main framework.
* [Gradle][3]: The build tool.
* [Spock][4]: The test framework.
* [Docker][5]: For building and running the application as containers.
* [QueryDSL][6]: For auto-generation of db models and running db migrations.

Pre-reqs
--------

To run this project you need:

* java8+ sdk installed.
* docker and docker-compose installed
* git ;)

Running
--------

To start the application just execute the script located at the root structure of this repo:

	$ ./start.sh

This script will:

* Build the project
* Execute the unit tests
* Build the docker image
* Start the database and deploy the application (docker-compose)

Note that if you are running it for the first time, it might take some time to download the dependencies.

To shut everything down just press ctrl+c.

Unit tests
-------

Most of the application constrains are tested with unit tests. See: `BookingServiceImplSpec`

Executing the concurrency test
-------

Make sure you have the application running. Open another terminal and just execute the following script located
also at the root structure of this repo:

	$ ./apitest.sh

This script will:

* Create a new directory (`test-results`) at the project root for the test reports
* Builds the docker image to run the postman collection as test scenario
* Execute the same test scenario 3 times in parallel
* Create the test report files in the `test-results` folder

Each test scenario does in this order:

1. Tries to create 3 reservation with no overlapping days
2. Get one reservation
3. Update one reservation
4. Delete one reservation
5. Get the availabilities for the next 30 days
6. Get the availabilities for a given date range

Note that the test cases order is unpredictable and the results might be different on every execution.

If 2 requests try to book the same date at the same time, the following database error log will be printed:

	db_1   | 2018-05-10 04:09:16.416 UTC [73] ERROR:  duplicate key value violates unique constraint "booking_date_unique_idx"
	db_1   | 2018-05-10 04:09:16.416 UTC [73] DETAIL:  Key (booked_date)=(2018-05-20) already exists.
	db_1   | 2018-05-10 04:09:16.416 UTC [73] STATEMENT:  insert into booking_date (booked_date, booking_id)

Also note that no error logs or stacktrace should be printed by the application (`app_1`) as the concurrent cases
are being handled.

Playing with the API
-------

The rest API was tested using Postman.

If you feel like trying out the API yourself, you can use Postman and import the
collection located at:

	<PROJECT_ROOT>/docker/campsite.postman_collection.json


[1]: https://swagger.io/
[2]: http://projects.spring.io/spring-boot/
[3]: http://gradle.org/
[4]: http://spockframework.github.io/spock/docs/1.0/index.html
[5]: https://www.docker.com/
[6]: http://www.querydsl.com/
