#!/usr/bin/env bash

hash docker 2>/dev/null || { echo "Docker not found!"; exit 1; }
hash docker-compose 2>/dev/null || { echo "Docker-compose not found!"; exit 1; }

./gradlew build
docker build -t campsite:local -f docker/Dockerfile .
docker-compose -f docker/docker-compose.yaml up
