#!/usr/bin/env bash

hash docker 2>/dev/null || { echo "docker not found!"; exit 1; }
hash docker-compose 2>/dev/null || { echo "docker-compose not found!"; exit 1; }

PWD=$(pwd)
TESTDIR=$PWD/test-results

if [ ! -d "$TESTDIR" ]; then
	mkdir $TESTDIR
fi

echo "Building apitest docker image..."
docker build -f docker/Dockerfile.apitest -t apitest:local --quiet .

echo "Running 3 parallel tests..."
docker run -v $TESTDIR:/test/newman --rm --network=host apitest:local -r html && echo "test1 done" &
docker run -v $TESTDIR:/test/newman --rm --network=host apitest:local -r html && echo "test2 done" &
docker run -v $TESTDIR:/test/newman --rm --network=host apitest:local -r html && echo "test3 done" &

wait
echo "Tests ran successfully! Please check the test-results directory to see the reports."

