package com.campsite.web.exception;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.campsite.booking.BookingValidationException;
import com.campsite.web.api.swagger.dto.ErrorResponse;

@Provider
public class BookingValidationExceptionMapper implements ExceptionMapper<BookingValidationException> {

	@Override
	public Response toResponse(BookingValidationException exception) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatus(BAD_REQUEST.getStatusCode());
		errorResponse.setMessage(exception.getMessage());

		return Response.status(BAD_REQUEST)
			.entity(errorResponse)
			.type(MediaType.APPLICATION_JSON)
			.build();
	}
}
