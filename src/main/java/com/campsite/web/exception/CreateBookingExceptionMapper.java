package com.campsite.web.exception;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.campsite.booking.CreateBookingException;
import com.campsite.web.api.swagger.dto.ErrorResponse;

@Provider
public class CreateBookingExceptionMapper implements ExceptionMapper<CreateBookingException> {

	@Override
	public Response toResponse(CreateBookingException exception) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatus(INTERNAL_SERVER_ERROR.getStatusCode());
		errorResponse.setMessage(exception.getMessage());

		return Response.status(INTERNAL_SERVER_ERROR)
			.entity(errorResponse)
			.type(MediaType.APPLICATION_JSON)
			.build();
	}
}
