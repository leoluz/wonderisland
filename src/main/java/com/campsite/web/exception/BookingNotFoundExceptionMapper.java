package com.campsite.web.exception;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.campsite.booking.BookingNotFoundException;
import com.campsite.web.api.swagger.dto.ErrorResponse;

@Provider
public class BookingNotFoundExceptionMapper implements ExceptionMapper<BookingNotFoundException> {

	@Override
	public Response toResponse(BookingNotFoundException exception) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatus(NOT_FOUND.getStatusCode());
		errorResponse.setMessage(exception.getMessage());

		return Response.status(NOT_FOUND)
			.entity(errorResponse)
			.type(MediaType.APPLICATION_JSON)
			.build();
	}
}
