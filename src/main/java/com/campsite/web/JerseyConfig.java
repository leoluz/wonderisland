package com.campsite.web;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.campsite.web.api.AvailabilitiesRestApi;
import com.campsite.web.api.BookingsRestApi;
import com.campsite.web.exception.BookingNotFoundExceptionMapper;
import com.campsite.web.exception.BookingValidationExceptionMapper;
import com.campsite.web.exception.CreateBookingExceptionMapper;

@Configuration
@ApplicationPath("/campsite")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		//Register apis
		register(BookingsRestApi.class);
		register(AvailabilitiesRestApi.class);

		//Register exception mappers
		register(BookingNotFoundExceptionMapper.class);
		register(BookingValidationExceptionMapper.class);
		register(CreateBookingExceptionMapper.class);
	}
}
