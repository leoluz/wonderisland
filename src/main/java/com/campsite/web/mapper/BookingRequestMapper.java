package com.campsite.web.mapper;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.campsite.booking.Booking;
import com.campsite.infra.mapper.MappableClass;
import com.campsite.web.api.swagger.dto.BookingRequest;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;

@Component
public class BookingRequestMapper implements MappableClass {

	@Override
	public void configure(MapperFactory factory) {
		factory.classMap(BookingRequest.class, Booking.class)
			.customize(new CustomMapper<BookingRequest, Booking>() {
				@Override
				public void mapAtoB(BookingRequest bookingRequest, Booking booking, MappingContext context) {
					LocalDate arrivalDate = LocalDate.parse(bookingRequest.getArrivalDate());
					LocalDate departureDate = LocalDate.parse(bookingRequest.getDepartureDate());
					long totalDays = DAYS.between(arrivalDate, departureDate) + 1;
					Set<LocalDate> days = Stream.iterate(arrivalDate, d -> d.plusDays(1))
						.limit(totalDays)
						.collect(Collectors.toSet());
					booking.setDates(new TreeSet<>(days));
				}
			})
			.byDefault()
			.register();
	}
}
