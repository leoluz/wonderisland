package com.campsite.web.mapper;

import org.springframework.stereotype.Component;

import com.campsite.booking.Booking;
import com.campsite.infra.mapper.MappableClass;
import com.campsite.web.api.swagger.dto.BookingResponse;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;

@Component
public class BookingResponseMapper implements MappableClass {

	@Override
	public void configure(MapperFactory factory) {
		factory.classMap(Booking.class, BookingResponse.class)
			.customize(new CustomMapper<Booking, BookingResponse>() {
				@Override
				public void mapAtoB(Booking booking, BookingResponse bookingResponse, MappingContext context) {
					bookingResponse.setArrivalDate(booking.getDates().first().toString());
					bookingResponse.setDepartureDate(booking.getDates().last().toString());
				}
			})
			.byDefault()
			.register();
	}
}
