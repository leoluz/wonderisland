package com.campsite.web.api;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.campsite.booking.BookingService;
import com.campsite.web.api.swagger.AvailabilitiesApi;
import com.campsite.web.api.swagger.dto.AvailabilitiesResponse;

@Component
public class AvailabilitiesRestApi extends AvailabilitiesApi {

    @Autowired
    private BookingService bookingService;

    @Override
    public Response doGetAvailabilities() {
		LocalDate today = LocalDate.now();
		Set<LocalDate> availableDates = bookingService.findAvailableDates(today, today.plusDays(30));
    	return Response.ok().entity(buildAvailabilitiesResponse(availableDates)).build();
    }

    @Override
    public Response doGetAvailabilitiesByDateRange(String fromDate, String toDate) {
		LocalDate startDate = LocalDate.parse(fromDate);
		LocalDate endDate = LocalDate.parse(toDate);
		Set<LocalDate> availableDates = bookingService.findAvailableDates(startDate, endDate);
    	return Response.ok().entity(buildAvailabilitiesResponse(availableDates)).build();
    }

	private AvailabilitiesResponse buildAvailabilitiesResponse(Set<LocalDate> dates) {
		List<String> availableDates = dates.stream().map(LocalDate::toString).collect(Collectors.toList());
		AvailabilitiesResponse availabilitiesResponse = new AvailabilitiesResponse();
		availabilitiesResponse.setAvailableDays(availableDates);
		return availabilitiesResponse;
	}
}

