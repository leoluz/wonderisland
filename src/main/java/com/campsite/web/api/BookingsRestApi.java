package com.campsite.web.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.campsite.booking.Booking;
import com.campsite.booking.BookingService;
import com.campsite.booking.BookingValidationException;
import com.campsite.infra.mapper.CoreMapper;
import com.campsite.web.api.swagger.BookingsApi;
import com.campsite.web.api.swagger.dto.BookingRequest;
import com.campsite.web.api.swagger.dto.BookingResponse;

@Component
public class BookingsRestApi extends BookingsApi {

	@Autowired
	private CoreMapper coreMapper;

	@Autowired
	private BookingService bookingService;

	@Override
	public Response doDeleteBookings(Integer bookingId) {
		bookingService.remove(bookingId);
		return Response.noContent().build();
	}

	@Override
	public Response doGetBookingsById(Integer bookingId) {
		Booking b = bookingService.findById(bookingId);
		return Response.ok().entity(coreMapper.map(b, BookingResponse.class)).build();
	}

	@Override
	public Response doPostBookings(BookingRequest bookingRequest) {
		validateBookingRequest(bookingRequest);
		Booking booking = coreMapper.map(bookingRequest, Booking.class);
		Integer id = bookingService.save(booking);
		return Response.created(getBookingsLocationURI(id)).build();
	}

	@Override
	public Response doPutBookings(Integer bookingId, BookingRequest bookingRequest) {
		validateBookingRequest(bookingRequest);
		Booking booking = coreMapper.map(bookingRequest, Booking.class);
		booking.setId(bookingId);
		bookingService.save(booking);
		return Response.noContent().build();
	}

	private void validateBookingRequest(BookingRequest bookingRequest) {
		LocalDate arrival = LocalDate.parse(bookingRequest.getArrivalDate());
		LocalDate departure = LocalDate.parse(bookingRequest.getDepartureDate());
		if (arrival.isAfter(departure)) {
			throw new BookingValidationException("Departure date needs to be after the arrival date");
		}
	}

	private URI getBookingsLocationURI(Integer id) {
		URI location = null;
		try {
			location = new URI("/campsite/bookings/"+id);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return location;
	}
}
