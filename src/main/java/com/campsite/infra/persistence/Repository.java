package com.campsite.infra.persistence;

import java.io.Serializable;
import java.util.Optional;

public interface Repository<T extends Persistable<I>, I extends Serializable> {
	I persist(T entity);

	Optional<T> findById(I id);

	boolean removeById(I id);

}

