package com.campsite.infra.persistence;

import java.io.Serializable;

import com.querydsl.core.types.Predicate;

public interface IdentifiablePredicate<I extends Serializable> {

	Predicate getByIdPredicate(I id);
}
