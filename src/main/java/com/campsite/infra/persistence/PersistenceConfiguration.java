package com.campsite.infra.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;

import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.SQLTemplatesRegistry;

@Configuration
public class PersistenceConfiguration {

	@Bean
	public SQLTemplates sqlTemplates(JdbcTemplate jdbcTemplate) {
		return jdbcTemplate.execute((ConnectionCallback<SQLTemplates>) (connection ->
			new SQLTemplatesRegistry().getTemplates(connection.getMetaData())));
	}
}
