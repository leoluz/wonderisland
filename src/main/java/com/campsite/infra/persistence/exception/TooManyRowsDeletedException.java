package com.campsite.infra.persistence.exception;

import static java.lang.String.format;

import java.io.Serializable;

public class TooManyRowsDeletedException extends PersistenceException {
	public <T, I extends Serializable> TooManyRowsDeletedException(Class<T> clazz, I id) {
		super(format("Too many rows affected when deleting %s by the id: %s", clazz, id));
	}
}
