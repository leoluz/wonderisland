package com.campsite.infra.persistence.exception;

import static java.lang.String.format;

import java.io.Serializable;

public class TooManyRowsUpdatedException extends PersistenceException {

	public <T, I extends Serializable> TooManyRowsUpdatedException(Class<T> clazz, I id) {
		super(format("Too many rows affected when updating %s by the id: %s", clazz, id));
	}
}
