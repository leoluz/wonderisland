package com.campsite.infra.persistence.exception;

import static java.lang.String.format;

import java.io.Serializable;

public class NoRowsUpdatedException extends RuntimeException {
	public <T, I extends Serializable> NoRowsUpdatedException(Class<T> clazz, I id) {
		super(format("No rows affected while updating entity: %s by the id: %s", clazz, id));
	}
}
