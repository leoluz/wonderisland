package com.campsite.infra.persistence.exception;

public class PersistenceException extends RuntimeException {

	private static final long serialVersionUID = 1381481398301369853L;

	public PersistenceException(String message) {
		super(message);
	}
}
