package com.campsite.infra.persistence;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.campsite.infra.mapper.CoreMapper;
import com.campsite.infra.persistence.exception.NoRowsUpdatedException;
import com.campsite.infra.persistence.exception.TooManyRowsDeletedException;
import com.campsite.infra.persistence.exception.TooManyRowsUpdatedException;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQueryFactory;

public abstract class DatabaseRepository<I extends Serializable, T extends Persistable<I>, M>
	implements Repository<T, I>, IdentifiablePredicate<I> {

	private RelationalPath<M> relationalPath;
	private Class<I> identifierClass;
	private Class<T> domainClass;
	private Class<M> modelClass;

	@Autowired
	private SQLQueryFactory queryFactory;
	@Autowired
	private CoreMapper coreMapper;

	public SQLQueryFactory getQueryFactory() {
		return queryFactory;
	}

	public DatabaseRepository(Class<I> identifierClass,
							  Class<T> domainClass,
							  Class<M> modelClass,
							  RelationalPath<M> relationalPath) {
		this.relationalPath = relationalPath;
		this.identifierClass = identifierClass;
		this.domainClass = domainClass;
		this.modelClass = modelClass;
	}

	@Override
	public Optional<T> findById(I id) {
		M model = queryFactory.selectFrom(relationalPath).where(getByIdPredicate(id)).fetchOne();
		return buildOptionalResult(model);
	}

	@Override
	public boolean removeById(I id) {
		boolean removed = false;
		long affectedRows = queryFactory.delete(relationalPath).where(getByIdPredicate(id)).execute();
		if (affectedRows > 1L) {
			throw new TooManyRowsDeletedException(modelClass, id);
		} else if (affectedRows == 1L) {
			removed = true;
		}
		return removed;
	}

	@Override
	public I persist(T entity) {
		M model = coreMapper.map(entity, modelClass);
		I id = null;
		if (entity.isNew()) {
			id = create(model);
		} else {
			id = entity.getId();
			update(model, id);
		}
		return id;
	}

	private I create(M model) {
		return queryFactory.insert(relationalPath).populate(model).executeWithKey(identifierClass);
	}

	private void update(M model, I id) {
		long affectedRows = queryFactory.update(relationalPath).populate(model).where(getByIdPredicate(id)).execute();
		if (affectedRows == 0L) {
			throw new NoRowsUpdatedException(modelClass, id);
		} else if (affectedRows > 1L) {
			throw new TooManyRowsUpdatedException(modelClass, id);
		}
	}

	protected Optional<T> buildOptionalResult(M model) {
		return Optional.ofNullable(model)
			.map(this::toDomain);
	}

	protected T toDomain(M model) {
		return coreMapper.map(model, domainClass);
	}
}
