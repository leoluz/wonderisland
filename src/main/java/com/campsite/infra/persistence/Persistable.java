package com.campsite.infra.persistence;

import java.io.Serializable;

public interface Persistable<I extends Serializable> {

	I getId();

	default boolean isNew() {
		boolean isNew = false;
		if (this.getId() == null) {
			isNew = true;
		}
		return isNew;
	}
}
