package com.campsite.infra.mapper;

import ma.glasnost.orika.MapperFactory;

/**
 * Defines the interface to be implemented by every Mapper.
 * A Mapper is an object that defines how to translate one java
 * bean into another.
 */
public interface MappableClass {
	/**
	 * This method should configure the translation between two
	 * java beans using the MapperFactory provided and at the end
	 * should register it.<br/>
	 * Ex:
	 * <blockquote>
	 *
	 * <pre>
	 * public void configure(MapperFactory factory)
	 * {
	 * 	factory.classMap(Party.class, PartiesDB.class).field(&quot;tenant&quot;, &quot;tenantId&quot;).byDefault().register();
	 * }
	 * </pre>
	 *
	 * </blockquote>
	 *
	 * @param factory should define how to map between two different
	 *           java beans.
	 * @see <a href="http://orika-mapper.github.io/orika-docs/mappings-via-classmapbuilder.html">Orika mapping
	 *      configuration</a>
	 */
	void configure(MapperFactory factory);
}
