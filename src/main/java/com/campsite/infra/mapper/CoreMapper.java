package com.campsite.infra.mapper;

import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
public class CoreMapper extends ConfigurableMapper {

	@Autowired
	private Collection<MappableClass> mappableClasses;
	private MapperFactory factory;

	@Override
	protected void configure(final MapperFactory mapperFactory) {
		this.factory = mapperFactory;
		final ConverterFactory converterFactory = factory.getConverterFactory();
//		converterFactory.registerConverter(new InstantTimestampConverter());
	}

	@PostConstruct
	void configureAllMappers() {
		mappableClasses.forEach(i -> i.configure(factory));
	}
}
