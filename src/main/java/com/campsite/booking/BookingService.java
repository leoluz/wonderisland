package com.campsite.booking;

import java.time.LocalDate;
import java.util.SortedSet;

public interface BookingService {
	public Booking findById(Integer id);

	public SortedSet<LocalDate> findAvailableDates(LocalDate startDate, LocalDate endDate);

	public Integer save(Booking booking);

	public boolean remove(Integer id);
}
