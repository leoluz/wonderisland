package com.campsite.booking;

public class CreateBookingException extends RuntimeException {
	public CreateBookingException(String msg) {
		super(msg);
	}
}
