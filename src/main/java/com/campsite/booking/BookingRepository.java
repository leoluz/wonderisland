package com.campsite.booking;

import static com.campsite.infra.db.generated.QBooking.booking;
import static com.campsite.infra.db.generated.QBookingDate.bookingDate;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.campsite.infra.db.generated.BookingDateModel;
import com.campsite.infra.db.generated.BookingModel;
import com.campsite.infra.mapper.CoreMapper;
import com.campsite.infra.persistence.DatabaseRepository;
import com.querydsl.core.QueryException;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Predicate;

@Repository
public class BookingRepository extends DatabaseRepository<Integer, Booking, BookingModel> {

	@Autowired
	private CoreMapper mapper;

	public BookingRepository() {
		super(Integer.class, Booking.class, BookingModel.class, booking);
	}

	@Override
	public Predicate getByIdPredicate(Integer id) {
		return booking.id.eq(id);
	}

	@Override
	public Optional<Booking> findById(Integer id) {

		List<Tuple> bookings = getQueryFactory().select(booking, bookingDate)
			.from(booking)
			.innerJoin(bookingDate).on(bookingDate.bookingId.eq(booking.id))
			.where(getByIdPredicate(id))
			.fetch();
		return Optional.ofNullable(toDomain(bookings));
	}

	public void deleteBookingDates(Integer bookingId) {
		getQueryFactory().delete(bookingDate).where(bookingDate.bookingId.eq(bookingId)).execute();
	}

	public void persistBookingDates(Integer bookingId, Set<LocalDate> dates) {
		dates.stream().forEach(date -> {
			BookingDateModel model= new BookingDateModel();
			model.setBookingId(bookingId);
			model.setBookedDate(Date.valueOf(date));
			try {
				getQueryFactory().insert(bookingDate).populate(model).execute();
			} catch (QueryException e) {
				throw new CreateBookingException(String.format("Error trying to reserve the date for booking %d", bookingId));
			}
		});
	}

	public SortedSet<LocalDate> findBookedDates(LocalDate startDate, LocalDate endDate) {
		List<BookingDateModel> results = getQueryFactory().selectFrom(bookingDate)
			.where(bookingDate.bookedDate.between(Date.valueOf(startDate), Date.valueOf(endDate)))
			.fetch();

		Set<LocalDate> dates = results.stream()
			.map(BookingDateModel::getBookedDate)
			.map(Date::toLocalDate)
			.collect(Collectors.toSet());

		return new TreeSet<>(dates);
	}

	private Booking toDomain(List<Tuple> tuples) {
		Booking b = null;
		if (tuples.size() > 0) {
			b = mapper.map(tuples.get(0).get(booking), Booking.class);
			Set<LocalDate> dates = tuples.stream()
				.map(t -> t.get(bookingDate))
				.map(BookingDateModel::getBookedDate)
				.map(Date::toLocalDate)
				.collect(Collectors.toSet());
			b.setDates(new TreeSet<>(dates));
		}
		return b;
	}
}
