package com.campsite.booking;

import java.time.LocalDate;
import java.util.Objects;
import java.util.SortedSet;

import com.campsite.infra.persistence.Persistable;

public class Booking implements Persistable<Integer>{

	private Integer id;
	private String name;
	private String email;
	private SortedSet<LocalDate> dates;

	public Booking() { }

	public Booking(Integer id, String name, String email, SortedSet<LocalDate> dates) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.dates = dates;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SortedSet<LocalDate> getDates() {
		return dates;
	}

	public void setDates(SortedSet<LocalDate> dates) {
		this.dates = dates;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Booking booking = (Booking) o;
		return Objects.equals(id, booking.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
