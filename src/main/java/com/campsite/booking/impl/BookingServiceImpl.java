package com.campsite.booking.impl;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.campsite.booking.Booking;
import com.campsite.booking.BookingNotFoundException;
import com.campsite.booking.BookingRepository;
import com.campsite.booking.BookingService;
import com.campsite.booking.BookingValidationException;

@Component
public class BookingServiceImpl implements BookingService {

	@Autowired
	private BookingRepository bookingRepository;

	@Transactional(readOnly = true)
	public Booking findById(Integer id) {
		return bookingRepository.findById(id).orElseThrow(()
			-> new BookingNotFoundException(String.format("Booking with id %d not found!", id)));
	}

	@Override
	@Transactional(readOnly = true)
	public SortedSet<LocalDate> findAvailableDates(LocalDate startDate, LocalDate endDate) {
		long totalDays = DAYS.between(startDate, endDate) + 1;
		Set<LocalDate> days = Stream.iterate(startDate, d -> d.plusDays(1))
			.limit(totalDays)
			.collect(Collectors.toSet());
		Set<LocalDate> bookedDates = bookingRepository.findBookedDates(startDate, endDate);
		Set<LocalDate> availableDays = days.stream().filter(day -> !bookedDates.contains(day)).collect(Collectors.toSet());
		return new TreeSet<>(availableDays);
	}

	@Override
	@Transactional
	public Integer save(Booking booking) {
		validateBooking(booking);

		if (booking.getId() != null) {
			//Make sure the booking exists
			findById(booking.getId());
			bookingRepository.deleteBookingDates(booking.getId());
		}
		Set<LocalDate> bookedDates = bookingRepository.findBookedDates(booking.getDates().first(), booking.getDates().last());
		if (bookedDates.size() > 0) {
			throw new BookingValidationException("Desired dates are already booked");
		}

		Integer id = bookingRepository.persist(booking);
		bookingRepository.persistBookingDates(id, booking.getDates());

		return id;
	}

	@Override
	@Transactional
	public boolean remove(Integer id) {
		bookingRepository.deleteBookingDates(id);
		return bookingRepository.removeById(id);
	}

	private void validateBooking(Booking booking) {
		if (booking.getDates() == null || booking.getDates().size() < 1) {
			throw new BookingValidationException("Reservation needs to contain at least one day");
		}
		if (booking.getDates().size() > 3) {
			throw new BookingValidationException("Reservation can not be made for more than 3 days");
		}
		if (booking.getDates().first().isBefore(LocalDate.now().plusDays(1))) {
			throw new BookingValidationException("Reservation must be made with minimum 1 day ahead of arrival");
		}
		if (booking.getDates().first().isAfter(LocalDate.now().plusDays(30))) {
			throw new BookingValidationException("Reservation can only be made up to 1 month of arrival");
		}
	}
}
