package com.campsite.booking;

public class BookingValidationException extends RuntimeException {
	public BookingValidationException(String message) {
		super(message);
	}
}
