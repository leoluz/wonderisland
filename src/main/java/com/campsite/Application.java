package com.campsite;

import javax.sql.DataSource;
import javax.validation.Validator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.spring.SpringConnectionProvider;

@SpringBootApplication
public class Application {
	public static void main (String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public SQLQueryFactory queryFactory(DataSource dataSource, SQLTemplates templates) {
		return new SQLQueryFactory(templates, new SpringConnectionProvider(dataSource));
	}

	@Bean
	public Validator validator() {
		return new LocalValidatorFactoryBean();
	}

}
