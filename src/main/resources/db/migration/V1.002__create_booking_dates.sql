CREATE TABLE IF NOT EXISTS booking_date (
	id serial PRIMARY KEY,
	booking_id INTEGER NOT NULL REFERENCES booking (id),
	booked_date date NOT NULL
);

CREATE INDEX booking_date_fk_idx on booking_date (booking_id)
