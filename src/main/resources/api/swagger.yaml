swagger: "2.0"
info:
  description: "Magic Island campsite booking API"
  version: "1.0.0"
  title: "Campsite Booking"
  contact:
    email: "campsite@campsite.com"
  license:
    name: "MIT License"
    url: "https://opensource.org/licenses/MIT"
host: "localhost:8080"
basePath: "/campsite"
tags:
- name: "campsite"
  description: "Campsite management API"
- name: "store"
  description: "Access to Petstore orders"
schemes:
- "http"
parameters:
  bookingId:
    name: "bookingId"
    in: "path"
    description: "ID of a booking"
    required: true
    type: "integer"
    format: "int32"
  bookingRequest:
    name: bookingRequest
    in: body
    required: true
    schema:
      $ref: '#/definitions/BookingRequest'
paths:
  /bookings:
    post:
      operationId: doPostBookings
      parameters:
        - $ref: '#/parameters/bookingRequest'
      tags:
      - "campsite"
      consumes:
      - "application/json"
      responses:
        201:
          description: "Booking created successfully"
          headers:
            Location:
              Schema:
                type: string
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
  /bookings/{bookingId}:
    get:
      operationId: doGetBookingsById
      parameters:
        - $ref: '#/parameters/bookingId'
      tags:
      - "campsite"
      produces:
      - "application/json"
      responses:
        200:
          description: "Get a booking details by id"
          schema:
            $ref: "#/definitions/BookingResponse"
        404:
          description: "Booking not found"
          schema:
              $ref: '#/definitions/ErrorResponse'
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
    put:
      operationId: doPutBookings
      parameters:
        - $ref: '#/parameters/bookingId'
        - $ref: '#/parameters/bookingRequest'
      tags:
      - "campsite"
      consumes:
      - "application/json"
      responses:
        204:
          description: "Booking updated successfully"
        404:
          description: "Booking not found"
          schema:
              $ref: '#/definitions/ErrorResponse'
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
    delete:
      operationId: doDeleteBookings
      parameters:
        - $ref: '#/parameters/bookingId'
      tags:
      - "campsite"
      responses:
        204:
          description: "Booking deleted successfully"
        404:
          description: "Booking not found"
          schema:
              $ref: '#/definitions/ErrorResponse'
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
  /availabilities:
    get:
      operationId: doGetAvailabilities
      tags:
      - "campsite"
      produces:
      - "application/json"
      summary: "Get the campsite availability for the next 30 days"
      responses:
        200:
          description: "The available days"
          schema:
            $ref: "#/definitions/AvailabilitiesResponse"
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
  /availabilities/{fromDate}/to/{toDate}:
    parameters:
      - name: "fromDate"
        in: "path"
        description: "starting date"
        required: true
        type: "string"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
      - name: "toDate"
        in: "path"
        description: "ending date"
        required: true
        type: "string"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
    get:
      operationId: doGetAvailabilitiesByDateRange
      tags:
      - "campsite"
      produces:
      - "application/json"
      summary: "Get the campsite availability for given date range"
      responses:
        200:
          description: "The available days"
          schema:
            $ref: "#/definitions/AvailabilitiesResponse"
        500:
          description: "Internal error"
          schema:
              $ref: '#/definitions/ErrorResponse'
definitions:
  BookingsResponse:
    type: array
    items:
      $ref: "#/definitions/BookingResponse"
  BookingResponse:
    type: object
    properties:
      id:
        type: "integer"
        format: "int32"
      name:
        type: "string"
      email:
        type: "string"
      arrivalDate:
        type: "string"
        description: "Format YYYY-MM-DD"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
      departureDate:
        type: "string"
        description: "Format YYYY-MM-DD"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
    example:
      id: 8924582349823
      name: "Ana Maria"
      email: "ana@maria.com"
      arrivalDate: "2018-07-01"
      departureDate: "2018-07-03"
  BookingRequest:
    type: object
    required:
      - name
      - email
      - arrivalDate
      - departureDate
    properties:
      name:
        type: "string"
      email:
        type: "string"
      arrivalDate:
        type: "string"
        description: "Format YYYY-MM-DD"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
      departureDate:
        type: "string"
        description: "Format YYYY-MM-DD"
        pattern: "\\d{4}-\\d{2}-\\d{2}"
    example:
      name: "Ana Maria"
      email: "ana@maria.com"
      arrivalDate: "2018-07-01"
      departureDate: "2018-07-03"
  AvailabilitiesResponse:
    type: object
    properties:
      availableDays:
        type: array
        items:
          type: string
          pattern: "\\d{4}-\\d{2}-\\d{2}"
    example:
      availableDays: ["2018-07-01", "2018-07-02", "2018-07-03", "2018-07-05", "2018-07-25"]
  ErrorResponse:
    type: object
    required:
      - status
      - message
    properties:
      status:
        type: integer
        description: "original HTTP error code, should be consistent with the response HTTP code"
        minimum: 100
        maximum: 599
      message:
        type: string
        description: "descriptive error message for debugging"
    example:
      status: 400
      message: "Missing required parameters"
