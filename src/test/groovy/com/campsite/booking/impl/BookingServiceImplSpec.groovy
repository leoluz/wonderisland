package com.campsite.booking.impl

import com.campsite.booking.Booking
import com.campsite.booking.BookingRepository
import com.campsite.booking.BookingService
import com.campsite.booking.BookingValidationException
import spock.lang.Specification

import java.time.LocalDate

class BookingServiceImplSpec extends Specification {

	BookingRepository bookingRepository = Mock()

	BookingService bookingService

	def setup() {
		bookingService = new BookingServiceImpl("bookingRepository": bookingRepository)
	}

	def "will find booking by id"() {
		given:
		def bookingId = 13
		def booking = buildBooking(bookingId, "Someone", "someone@fake.com")
		bookingRepository.findById(bookingId) >> Optional.of(booking)

		when:
		Booking found = bookingService.findById(bookingId)

		then:
		found != null
		found.equals(booking)
	}

	def "will find available dates"() {
		given:
		def startDate = LocalDate.of(2018, 10, 10)
		def endDate = LocalDate.of(2018, 10, 15)
		def reserved1 = LocalDate.of(2018, 10, 11)
		def reserved2 = LocalDate.of(2018, 10, 12)
		def reserved3 = LocalDate.of(2018, 10, 13)
		SortedSet<LocalDate> reservedDates = [reserved1, reserved2, reserved3] as SortedSet
		bookingRepository.findBookedDates(startDate, endDate) >> reservedDates

		when:
		Set<LocalDate> availableDays = bookingService.findAvailableDates(startDate, endDate)

		then:
		availableDays.size() == 3
		availableDays.getAt(0) == LocalDate.of(2018, 10, 10)
		availableDays.getAt(1) == LocalDate.of(2018, 10, 14)
		availableDays.getAt(2) == LocalDate.of(2018, 10, 15)
	}

	def "will save new booking successfully"() {
		given:
		def bookingId = 50
		def booking = buildBooking(null, "New Booking", "new@fake.com")
		def date1 = LocalDate.now().plusDays(3)
		def date2 = LocalDate.now().plusDays(4)
		def date3 = LocalDate.now().plusDays(5)
		SortedSet<LocalDate> bookingDates = [date1, date2, date3] as SortedSet
		booking.setDates(bookingDates)
		SortedSet<LocalDate> noBookedDates = [] as SortedSet
		bookingRepository.findBookedDates(booking.getDates().first(), booking.getDates().last()) >> noBookedDates
		bookingRepository.persist(booking) >> bookingId

		when:
		Integer id = bookingService.save(booking)

		then:
		id != null
		id == bookingId
	}

	def "Reservation needs to contain at least one day"() {
		given:
		def booking = buildBooking(null, "New Booking", "new@fake.com")

		when:
		bookingService.save(booking)

		then:
		thrown(BookingValidationException)
	}

	def "Reservation can not be made for more than 3 days"() {
		def booking = buildBooking(null, "New Booking", "new@fake.com")
		def date1 = LocalDate.now().plusDays(3)
		def date2 = LocalDate.now().plusDays(4)
		def date3 = LocalDate.now().plusDays(5)
		def date4 = LocalDate.now().plusDays(6)
		SortedSet<LocalDate> bookingDates = [date1, date2, date3, date4] as SortedSet
		booking.setDates(bookingDates)

		when:
		bookingService.save(booking)

		then:
		thrown(BookingValidationException)
	}

	def "Reservation must be made with minimum 1 day ahead of arrival"() {
		def booking = buildBooking(null, "New Booking", "new@fake.com")
		def date1 = LocalDate.now()
		SortedSet<LocalDate> bookingDates = [date1] as SortedSet
		booking.setDates(bookingDates)

		when:
		bookingService.save(booking)

		then:
		thrown(BookingValidationException)
	}

	def "Reservation can only be made up to 1 month of arrival"() {
		def booking = buildBooking(null, "New Booking", "new@fake.com")
		def date1 = LocalDate.now().plusDays(32)
		SortedSet<LocalDate> bookingDates = [date1] as SortedSet
		booking.setDates(bookingDates)

		when:
		bookingService.save(booking)

		then:
		thrown(BookingValidationException)
	}

	def buildBooking(bookingId, bookingName, bookingEmail) {
		def booking = new Booking()
		booking.with {
			id = bookingId
			name = bookingName
			email = bookingEmail
		}
		booking
	}
}
